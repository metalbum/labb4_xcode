//
//  ViewController.m
//  PongGame
//
//  Created by IT-Högskolan on 2015-02-09.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property float screenWidth;
@property float screenHeight;

@property float X;
@property float Y;

@property int playerScore;
@property int computerScore;

@property float computerSpeed;
@property int ballSpeed;

@property (weak, nonatomic) IBOutlet UIView *playerPaddle;
@property (weak, nonatomic) IBOutlet UIView *computerPaddle;
@property (weak, nonatomic) IBOutlet UIView *ball;

@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property NSTimer *countdownTimer;
@property NSTimer *mainTimer;

@property AVAudioPlayer *playComputerPaddleSound;
@property AVAudioPlayer *playPlayerPaddleSound;
@property AVAudioPlayer *playLostSound;
@property AVAudioPlayer * playHitWallSound;

@property (weak, nonatomic) IBOutlet UIView *leftWall;
@property (weak, nonatomic) IBOutlet UIView *rightWall;
@property (weak, nonatomic) IBOutlet UILabel *playerScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *computerScoreLabel;

@end

@implementation ViewController


- (IBAction)playButtonPressed:(id)sender {
    
    self.playButton.hidden = YES;
    
    self.ball.center = CGPointMake(self.screenWidth / 2, self.screenHeight / 2);
    
    [self releaseBall];
    
    self.mainTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(moveBall) userInfo:nil repeats:YES];
    
    
}

-(void)releaseBall {
    int upOrDown = arc4random() % 2;
    
    if (upOrDown == 0){
        self.X = self.ballSpeed;
    } else {
        self.X = -self.ballSpeed;
    }
    
    int leftOrRight = arc4random() % 2;
    
    if (leftOrRight == 0){
        self.Y = self.ballSpeed;
    } else {
        self.Y = -self.ballSpeed;
    }
    
    [self moveBall];
}

-(void)moveComputerPaddle {
        
        if (self.ball.center.x < self.computerPaddle.center.x &&
            !CGRectIntersectsRect(self.computerPaddle.frame, self.leftWall.frame)) {
            self.computerPaddle.center = CGPointMake(self.computerPaddle.center.x - self.computerSpeed, self.computerPaddle.center.y);
        } else if (self.ball.center.x > self.computerPaddle.center.x &&
                   !CGRectIntersectsRect(self.computerPaddle.frame, self.rightWall.frame)) {
            self.computerPaddle.center = CGPointMake(self.computerPaddle.center.x + self.computerSpeed, self.computerPaddle.center.y);
        }
    
    
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *drag = [[event allTouches] anyObject];
    self.playerPaddle.center = [drag locationInView:self.view];
    
    if (self.playerPaddle.center.x != self.screenHeight - 50) {
        self.playerPaddle.center = CGPointMake(self.playerPaddle.center.x, self.screenHeight - 50);
    }
}

-(void)moveBall{
    
    self.ball.center = CGPointMake(self.ball.center.x - self.X, self.ball.center.y + self.Y);
    
    [self moveComputerPaddle];
    
    if ([self impactWall]){
        [self.playHitWallSound play];
        self.X = 0 - self.X;
    } else if ([self impactPaddle]){
        
        if (self.ball.center.y < self.screenHeight / 2) {
            [self.playComputerPaddleSound play];
        } else {
            [self.playPlayerPaddleSound play];
        }
        
        [self calculateVelocity];
        self.Y = 0 - self.Y;
    } else {
        [self checkWhoLost];
    }
    
}

-(void)calculateVelocity {
    
    if (self.ball.center.y < self.screenHeight / 2) {
        
        float value = self.computerPaddle.center.x - self.ball.center.x;
        self.X = value / 10;
    } else {
        
        float value = self.playerPaddle.center.x - self.ball.center.x;
        self.X = value / 10;
    }
}

-(void)checkWhoLost {
    
    if (self.ball.center.y < 0) {
        self.playerScore++;
        self.playerScoreLabel.text = [NSString stringWithFormat:@"%d", self.playerScore];
        [self checkLoser];
        
    } else if (self.ball.center.y > self.screenHeight) {
        self.computerScore++;
        self.computerScoreLabel.text = [NSString stringWithFormat:@"%d", self.computerScore];
        [self checkLoser];
    }
}

-(void)checkLoser {
    [self.playLostSound play];
    [self.mainTimer invalidate];
    self.playButton.hidden = NO;
}

-(bool)impactWall{
    return CGRectIntersectsRect(self.ball.frame, self.leftWall.frame) ||
        CGRectIntersectsRect(self.ball.frame, self.rightWall.frame);
}

-(bool)impactPaddle{
    
    return CGRectIntersectsRect(self.ball.frame, self.playerPaddle.frame) ||
        CGRectIntersectsRect(self.ball.frame, self.computerPaddle.frame);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *computerPaddleSoundPath = [NSString stringWithFormat:@"%@/hitComputerPaddle.mp3", [[NSBundle mainBundle] resourcePath]];
    NSString *playerPaddleSoundPath = [NSString stringWithFormat:@"%@/hitPlayerPaddle.mp3", [[NSBundle mainBundle] resourcePath]];
    NSString *lostSoundPath = [NSString stringWithFormat:@"%@/lost.mp3", [[NSBundle mainBundle] resourcePath]];
    NSString *wallSoundPath = [NSString stringWithFormat:@"%@/hitWall.mp3", [[NSBundle mainBundle] resourcePath]];
    
    NSURL *computerPaddleSoundURL = [NSURL fileURLWithPath:computerPaddleSoundPath];
    NSURL *playerPaddleSoundURL = [NSURL fileURLWithPath:playerPaddleSoundPath];
    NSURL *lostSoundURL = [NSURL fileURLWithPath:lostSoundPath];
    NSURL *wallSoundURL = [NSURL fileURLWithPath:wallSoundPath];
    
    self.playComputerPaddleSound = [[AVAudioPlayer alloc] initWithContentsOfURL:computerPaddleSoundURL error:nil];
    self.playPlayerPaddleSound = [[AVAudioPlayer alloc] initWithContentsOfURL:playerPaddleSoundURL error:nil];
    self.playLostSound = [[AVAudioPlayer alloc] initWithContentsOfURL:lostSoundURL error:nil];
    self.playHitWallSound = [[AVAudioPlayer alloc] initWithContentsOfURL:wallSoundURL error:nil];
    
    [self.playComputerPaddleSound prepareToPlay];
    [self.playPlayerPaddleSound prepareToPlay];
    [self.playLostSound prepareToPlay];
    [self.playHitWallSound prepareToPlay];
    
    self.playerScore = 0;
    self.computerScore = 0;
    
    self.computerSpeed = 2;
    self.ballSpeed = 5;
}

- (void)viewDidLayoutSubviews {
    self.screenWidth = self.view.frame.size.width;
    self.screenHeight = self.view.frame.size.height;
    
    self.leftWall.frame = CGRectMake(0, 0, 15, self.screenHeight);
    self.rightWall.frame = CGRectMake(self.screenWidth - 15, 0, 15, self.screenHeight);
    
    self.computerPaddle.frame = CGRectMake(self.screenWidth / 2, 50, self.screenWidth / 5, 10);
    self.playerPaddle.frame = CGRectMake(self.screenWidth / 2, (self.screenHeight - 50) - (self.playerPaddle.frame.size.height / 2), self.screenWidth / 5, 10);
    
    self.ball.center = CGPointMake(self.screenWidth / 2, self.screenHeight / 2);
    
    self.computerScoreLabel.center = CGPointMake(30, 30);
    self.playerScoreLabel.center = CGPointMake(self.screenWidth - 25, self.screenHeight - 30);
    
    self.playButton.bounds = CGRectMake(0, 0, self.screenWidth, self.screenHeight);
    self.playButton.center = CGPointMake(self.screenWidth / 2, self.screenHeight / 2);
    
    [self.view bringSubviewToFront:self.playButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
