//
//  MyUiView.m
//  PongGame
//
//  Created by IT-Högskolan on 2015-02-09.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "MyUiView.h"

@implementation UIView (MyUiView)

-(void)adjustWithBorder:(float)border {
    if (self.superview) {
        CGRect newFrame = CGRectMake(0.0f, 0.0f, self.superview.frame.size.width - border * 2.0f, self.superview.frame.size.height - border * 2.0f);
        self.frame = newFrame;
        self.center = self.superview.center;
    }
}

@end
